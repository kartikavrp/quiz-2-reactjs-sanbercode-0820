// soal 12
 
class BangunDatar {
	constructor(nama) {
		this._nama = nama;
	}

	luas() {
		return console.log(" ");
	}

	keliling() {
		return console.log(" ");
	}
}

class Lingkaran extends BangunDatar {
	constructor(nama, r) {
    	super(nama);
    	this._r= r;
    	this._pi = 22/7;
    }

    get r() {
    	return this._r;
    }

    set r(x) {
    	this._r = x;
    }    

    luas() {  	
    	let luas = this._pi*this.r*this.r;
    	return console.log("Luas lingkaran = ", luas);
	}

	keliling() {
		let keliling = 2*this._pi*this.r;
		return console.log("Keliling lingkaran = ",keliling);
	}
}

class Persegi extends BangunDatar {
	constructor(nama, s) {
    	super(nama);
    	this._sisi= s
    }

    get s() {
    	return this._sisi;
    }

    set s(x) {
    	this._sisi= x;
    }

    luas() {
    	let luas = this.s*this.s;
    	return console.log("Luas persegi = ",luas);
	}

	keliling() {
		let keliling = 4*this.s;
		return console.log("Keliling persegi = ", keliling);
	}
}

var bentuk = new BangunDatar("segitiga");
bentuk.luas;
bentuk.keliling;

var bulat = new Lingkaran("lingkaran 1", 7);
bulat.luas();
bulat.keliling();

var kotak = new Persegi("persegi 1", 5);
kotak.luas();
kotak.keliling();

