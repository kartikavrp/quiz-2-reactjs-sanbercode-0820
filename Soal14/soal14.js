// soal 14

const volBalok = (panjang, lebar, tinggi) => {
	let p = panjang;
	let l = lebar;
	let t = tinggi;

	let volume = p*l*t;
    return `Volume Balok dengan p = ${p}, l = ${l}, dan t = ${t} adalah ${volume}`;
}

const volKubus = (sisi) => {
	let s = sisi;
	let volume = s*s*s;
    return `Volume Kubus dengan s = ${s} adalah ${volume}`;
}

// panggil Function
console.log(volBalok(6,4,3));
console.log(volKubus(4));

//buatlah arrow function volume balok dan kubus, 
//gunakan rest parameter di parameter functionnya. 
//lalu tampilkan hasil perhitungannya dengan template literal

