import React from 'react';

class Name extends React.Component {
  render() {
    return <b>{this.props.name}</b>;
  }
}

class Age extends React.Component {
  render() {
    return <p>{this.props.age} years old</p> ;
  }
}

class Call extends React.Component {
  render() {
  	let call="";
  	if (this.props.gender=="Male") {
        return call="Mr. ";
    } else if (this.props.gender=="Female") {
        return call="Mrs. "
    }
    return <b>call</b>;
  }
}

class Profession extends React.Component {
  render() {
    return <p>{this.props.profession}</p>;
  }
}

class Photo extends React.Component {
  render() {	
    return <img src="{this.props.photo}" /> ;
  }
}

const data = [
	{name: "John", age: 25, gender: "Male", profession: "Engineer", photo: "https://media.istockphoto.com/photos/portarit-of-a-handsome-older-man-sitting-on-a-sofa-picture-id1210237745"}, 
	{name: "Sarah", age: 22, gender: "Female", profession: "Designer", photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"}, 
	{name: "David", age: 30, gender: "Male", profession: "Programmer", photo: "https://media.istockphoto.com/photos/handsome-mexican-hipster-man-sending-email-with-laptop-picture-id1182472756"}, 
	{name: "Kate", age: 27, gender: "Female", profession: "Model", photo: "https://cdn.pixabay.com/photo/2015/05/17/20/07/fashion-771505_960_720.jpg" }
];

class PrintData extends React.Component {
  render() {
    return (
      <>
        {data.map(el=> {
          return (
          	<div className="data-container">     
              <Photo photo={el.photo}/> <br/>
              <Call gender={el.gender}/> <Name name={el.name}/>
              <Profession profession={el.profession}/>
              <Age age={el.age}/>                         
            </div>
	        
          )
        })}
      </>
    )
  }
}

export default PrintData